@extends('adminlte.master')

@section('content')

<div class="ml-7 mt-7">
    <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Buat Pertanyaan Baru</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="/pertanyaan" method="POST">
                @csrf
                    <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Judul</label>
                        <input type="text" class="form-control" id="judul" value="{{ old('judul', '')}}" name="judul" placeholder="judul pertanyaan">
                        @error('judul')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Isi Pertanyaan</label>
                        <textarea class="form-control" rows="3" id="isi" value="{{ old('isi', '')}}" name="isi" placeholder="isi pertanyaan"></textarea>
                        @error('isi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    <!--   <input type="text" class="form-control" id="body" name="body" placeholder="isi pertanyaan"> -->
                    </div>
                    
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
    </div>
</div>
@endsection